// WARNING: 增加方法谨防覆盖 gl-matrix 的内置方法。
import * as mat3 from './mat3';
import * as vec2 from './vec2';
import * as vec3 from './vec3';

export { mat3, vec2, vec3 };
